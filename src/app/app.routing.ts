import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    loadChildren: () => import('./home/home.module').then(mod => mod.HomeModule)
  },
];

export const AppRoutes = RouterModule.forRoot(routes);
